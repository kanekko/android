package mx.unam.holamundo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends Activity {

    TextView txtNombre;
    Bundle params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        txtNombre = (TextView) findViewById(R.id.txtNombre);
        params = getIntent().getExtras();

        txtNombre.setText("Hola\n" + params.getString("nombreUser"));
    }
}
