package mx.unam.holamundo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    TextView txtChange;
    Button miBtn;
    EditText miEdtx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtChange = (TextView) findViewById(R.id.miTxtCuatro);
        miBtn = (Button) findViewById(R.id.btn_siguiente);
        miEdtx = (EditText) findViewById(R.id.edtxInfo);

        miBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = miEdtx.getText().toString();
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("nombreUser", nombre);
                startActivity(intent);
            }
        });

        txtChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtChange.setText("Ya cambié de valor, HOLA!");
            }
        });
    }

}
