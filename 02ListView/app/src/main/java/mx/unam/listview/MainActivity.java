package mx.unam.listview;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    ListView listView;
    EditText edtxSearch;
    int b = 0;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.miListView);
        edtxSearch = (EditText) findViewById(R.id.miEditText);

        final String[] data = new String[] {
                "Jelly Bean", "Ice Cream Sandwich", "Linux", "Windows", "MacOS", "UNIX", "Android",
                "Jelly Bean", "Ice Cream Sandwich", "Linux", "Windows", "MacOS", "UNIX", "Android",
                "Jelly Bean", "Ice Cream Sandwich", "Linux", "Windows", "MacOS", "UNIX", "Android"
        };
        final String[] dataReplace = new String[100];
        dataReplace[0] = "";

        adapter = new ArrayAdapter<String>(MainActivity.this,
                                           android.R.layout.simple_list_item_1,
                                           data);
        listView.setAdapter(adapter);

        edtxSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.e("Before", charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.e("onChanged", charSequence.toString());
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.e("After",editable.toString());
                if (editable.toString().length() != 0){
                    for (int i=0; i<data.length; i++){
                        if (data[i].contains(editable)){
                            b++;
                            dataReplace[b] = data[i];
                            Log.e("dataReplace", dataReplace[b]);
                        }
                    }
                }
                b=0;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView txtView = (TextView) view.findViewById(android.R.id.text1); // para obtener el valor en listas personalizadas.
                //String valor = (String) listView.getItemAtPosition(i); //para obtener el valor en Listas sencillas.
                //Toast.makeText(MainActivity.this, "El valor es: "+txtV.getText().toString(), Toast.LENGTH_SHORT).show();
                //txtV.setText("IO");

                if (i == 9){
                    Toast.makeText(MainActivity.this, "El valor es: "+txtView.getText().toString(), Toast.LENGTH_SHORT).show();
                }
                if (i == 5){
                    lanzarAlerta(txtView);
                }
                if (i == 1){
                    startActivity(new Intent(MainActivity.this, MiVistaActivity.class));
                }
            }
        });
    }


    /**
     * @param textView textView
     */
    private void lanzarAlerta(TextView textView){
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);

        alertBuilder.setTitle("Mi alerta");
        alertBuilder.setMessage(textView.getText().toString());

        alertBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                MainActivity.this.finish();
            }
        });
        alertBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.e("mi botón cancelar", "press");
            }
        });
        alertBuilder.setNeutralButton("Nada", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.e("mi botón neutral", "press");
            }
        });
        alertBuilder.setIcon(R.mipmap.ic_launcher);

        AlertDialog dialog = alertBuilder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        //Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
