package mx.unam.usingmaterial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button miBtn;
    EditText miEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        miBtn = (Button) findViewById(R.id.miBoton);
        miEdit = (EditText) findViewById(R.id.miEdit);

        miBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                miEdit.setError(null);
                if(miEdit.getText().toString().length() == 0){
                    //miEdit.setError("Ingresa una palabra");
                    miEdit.setError(getString(R.string.hello_world));
                } else {
                  startActivity( new Intent(MainActivity.this, NavegadorActivity.class).putExtra("data", miEdit.getText().toString()) );
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_settings){
            return  true;
        }

        return super.onOptionsItemSelected(item);
    }
}
