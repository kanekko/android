package mx.unam.usingmaterial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class NavegadorActivity extends AppCompatActivity {

    WebView miWebView;
    ProgressBar miProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navegador);

        miWebView = (WebView) findViewById(R.id.miWebView);
        miProgress = (ProgressBar) findViewById(R.id.miProgress);

        miWebView.getSettings().setJavaScriptEnabled(true);
        miWebView.loadUrl("https://www.youtube.com/results?search_query="+getIntent().getExtras().getString("data"));

        miWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                return false;
            }
        });

        miWebView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress){
                miProgress.setProgress(0);
                miProgress.setVisibility(View.VISIBLE);

                NavegadorActivity.this.setProgress(newProgress*1000);
                miProgress.incrementProgressBy(newProgress);

                if(newProgress == 100){
                    miProgress.setVisibility(View.GONE);
                }
            }
        });

    }
}
