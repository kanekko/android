package mx.unam.tiposdemensajes;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{

    TextView miTextView;
    ImageView miImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        miTextView = (TextView) findViewById(R.id.miTextView);
        miImageView = (ImageView) findViewById(R.id.miImg);

        miTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                miTextView.setText("Cambiando el texto");
                Log.e("TextView", "Ya cambie de texto");
            }
        });

        miImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                miImageView.setImageResource(R.drawable.imagen_cubo);
                Toast.makeText(MainActivity.this,
                               "Cualquier notificación al usuario",
                               Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * método con listener directo en la vista
     * @param view View
     */
    public void lanzaToast(View view) {
        Toast.makeText(MainActivity.this,
                       "Lanzando desde el botón",
                       Toast.LENGTH_LONG).show();
        miTextView.setText("Cambiando desde el botón");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgButton:
                Log.d("imgButton", "usando onClick con switch");
                Toast.makeText(MainActivity.this,
                               "imageButton",
                               Toast.LENGTH_SHORT).show();
                break;
        }
    }

}
