package mx.unam.androidmenus;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                Log.e("action uno", "item 1");
                break;
            case R.id.action_settings_dos:
                Log.e("action dos", "item 2");
                break;
            case R.id.action_menu_visible:
                Log.e("action menu", "item 3");
                break;
            case R.id.action_menu_visible_dos:
                Log.e("action menu dos", "item 4");
                break;
            case R.id.action_menu_visible_tres:
                Log.e("action menu tres", "item 5");
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
