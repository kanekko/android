package mx.unam.singleton;

public enum MiSingleton {
    SINGLETON;

    private static String name;
    private static String lastname;
    private static String age;

    //name
    public static String getName(){
        return name;
    }

    public static void setName(String name){
        MiSingleton.name = name;
    }

    //lastname
    public static String getLastname(){
        return lastname;
    }

    public static void setLastname(String lastname){
        MiSingleton.lastname = lastname;
    }

    //age
    public static String getAge(){
        return age;
    }

    public static void setAge(String age){
        MiSingleton.age = age;
    }
}
